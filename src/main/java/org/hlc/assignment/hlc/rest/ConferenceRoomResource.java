package org.hlc.assignment.hlc.rest;


import org.hlc.assignment.hlc.entity.ConferenceRoom;
import org.hlc.assignment.hlc.repository.ConferenceRoomRepository;
import org.hlc.assignment.hlc.restutil.BadRequestAlertException;
import org.hlc.assignment.hlc.restutil.HeaderUtil;
import org.hlc.assignment.hlc.restutil.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ConferenceRoomResource {

    private final Logger log = LoggerFactory.getLogger(ConferenceRoomResource.class);

    private static final String ENTITY_NAME = "conferenceRoom";

    private final ConferenceRoomRepository conferenceRoomRepository;

    public ConferenceRoomResource(ConferenceRoomRepository conferenceRoomRepository) {
        this.conferenceRoomRepository = conferenceRoomRepository;
    }


    @PostMapping("/conference-rooms")
    public ResponseEntity<ConferenceRoom> createConferenceRoom(@Valid @RequestBody ConferenceRoom conferenceRoom) throws URISyntaxException {
        log.debug("REST request to save ConferenceRoom : {}", conferenceRoom);
        if (conferenceRoom.getId() != null) {
            throw new BadRequestAlertException("A new conferenceRoom cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConferenceRoom result = conferenceRoomRepository.save(conferenceRoom);
        return ResponseEntity.created(new URI("/api/conference-rooms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/conference-rooms")
    public ResponseEntity<ConferenceRoom> updateConferenceRoom(@Valid @RequestBody ConferenceRoom conferenceRoom) throws URISyntaxException {
        log.debug("REST request to update ConferenceRoom : {}", conferenceRoom);
        if (conferenceRoom.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ConferenceRoom result = conferenceRoomRepository.save(conferenceRoom);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, conferenceRoom.getId().toString()))
            .body(result);
    }

    @GetMapping("/conference-rooms")
    public List<ConferenceRoom> getAllConferenceRooms() {
        log.debug("REST request to get all ConferenceRooms");
        return conferenceRoomRepository.findAll();
    }


    @GetMapping("/conference-rooms/{id}")
    public ResponseEntity<ConferenceRoom> getConferenceRoom(@PathVariable Long id) {
        log.debug("REST request to get ConferenceRoom : {}", id);
        Optional<ConferenceRoom> conferenceRoom = conferenceRoomRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(conferenceRoom);
    }


    @DeleteMapping("/conference-rooms/{id}")
    public ResponseEntity<Void> deleteConferenceRoom(@PathVariable Long id) {
        log.debug("REST request to delete ConferenceRoom : {}", id);

        conferenceRoomRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
