package org.hlc.assignment.hlc.rest;


import org.hlc.assignment.hlc.dto.BookingDTO;
import org.hlc.assignment.hlc.entity.Booking;
import org.hlc.assignment.hlc.entity.ConferenceRoom;
import org.hlc.assignment.hlc.repository.BookingRepository;
import org.hlc.assignment.hlc.repository.ConferenceRoomRepository;
import org.hlc.assignment.hlc.restutil.BadRequestAlertException;
import org.hlc.assignment.hlc.restutil.CustomParameterizedException;
import org.hlc.assignment.hlc.restutil.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class BookingResource {

    private final Logger log = LoggerFactory.getLogger(BookingResource.class);

    private static final String ENTITY_NAME = "booking";

    private static final String ENTITY_NAME_CONFERENCE_ROOM = "booking";

    private final BookingRepository bookingRepository;
    private final ConferenceRoomRepository conferenceRoomRepository;


    public BookingResource(BookingRepository bookingRepository, ConferenceRoomRepository conferenceRoomRepository) {
        this.bookingRepository = bookingRepository;
        this.conferenceRoomRepository = conferenceRoomRepository;
    }

    @PostMapping("/bookings")
    public ResponseEntity<Booking> createBooking(@Valid @RequestBody BookingDTO bookingDTO) throws URISyntaxException, CustomParameterizedException {
        final Optional<ConferenceRoom> conferenceRoom = conferenceRoomRepository.findById(bookingDTO.getConferenceRoomId());
        if (!conferenceRoom.isPresent()) {
            throw new BadRequestAlertException("Invalid id of conferenceRoom ", ENTITY_NAME_CONFERENCE_ROOM, "idnull");
        }
        Booking booking = null;
        Booking latestBooking = bookingRepository.findFirstByConferenceRoomIdOrderByEndDateDesc(bookingDTO.getConferenceRoomId());
        if (bookingDTO.getStartDate().isAfter(latestBooking.getEndDate())) {
            booking = new Booking();
            booking.setConferenceRoom(conferenceRoom.get());
            booking.setStartDate(bookingDTO.getStartDate());
            booking.setEndDate(booking.getEndDate());
            booking.setRequestedBy(bookingDTO.getRequestedBy());
        } else {
            throw new CustomParameterizedException("Conference room is engaged , currently not available");
        }
        Booking result = bookingRepository.save(Objects.requireNonNull(booking));
        return ResponseEntity.created(new URI("/api/bookings/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
}
