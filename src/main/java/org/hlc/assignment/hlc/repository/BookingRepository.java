package org.hlc.assignment.hlc.repository;


import org.hlc.assignment.hlc.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

@SuppressWarnings("unused")
@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    Booking findFirstByConferenceRoomIdOrderByEndDateDesc(@PathVariable("id") Long id);
}
