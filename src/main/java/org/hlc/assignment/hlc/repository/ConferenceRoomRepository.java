package org.hlc.assignment.hlc.repository;

import org.hlc.assignment.hlc.entity.ConferenceRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoom, Long> {

}
