package org.hlc.assignment.hlc;

/*
*
* CREATED BY: MOSHIOUR RAHMAN
*
* */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*
* HLC wants to develop a software by which we are able to manage the
 * bookings of conference rooms. HLC is present in multiple cities and each city
 * has multiple offices. Each office has multiple floors, and each floor
 * can have 0 or more conference rooms.
 * Design the system where a user can book any of the conference rooms online.
*
* develop a REST based application with below possibilities:
		  1. CRUD operations on conference rooms
		  2. A person should be able to book a conference room if it is available.
*
* */

@SpringBootApplication
public class HlcApplication {

    public static void main(String[] args) {
        SpringApplication.run(HlcApplication.class, args);
    }
}
