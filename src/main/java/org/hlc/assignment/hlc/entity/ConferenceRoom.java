package org.hlc.assignment.hlc.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "conference_room")
public class ConferenceRoom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "room_name", nullable = false)
    private String roomName;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private Floor floor;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private City city;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private Office office;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public ConferenceRoom roomName(String roomName) {
        this.roomName = roomName;
        return this;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Floor getFloor() {
        return floor;
    }

    public ConferenceRoom floor(Floor floor) {
        this.floor = floor;
        return this;
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }

    public City getCity() {
        return city;
    }

    public ConferenceRoom city(City city) {
        this.city = city;
        return this;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Office getOffice() {
        return office;
    }

    public ConferenceRoom office(Office office) {
        this.office = office;
        return this;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConferenceRoom conferenceRoom = (ConferenceRoom) o;
        if (conferenceRoom.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), conferenceRoom.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConferenceRoom{" +
            "id=" + getId() +
            ", roomName='" + getRoomName() + "'" +
            "}";
    }
}
